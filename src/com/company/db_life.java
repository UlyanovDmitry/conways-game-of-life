package com.company;
import sun.font.GlyphLayout;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

/**
 * Created by ulyan on 05.04.2016.
 */
public class db_life extends Life {

    private int EMPTY = 0;
    private int ALIVE = 1;
    private int GONNA_DIE = 2;
    private int GONNA_LIVE = 4;

    private int UPPER_LIVE_BORDER = 3;
    private int LOWER_LIVE_BORDER = 2;

    private int BORN_BORDER = 3;

    private static final String url = "jdbc:mysql://localhost:3306/life_database";
    private static final String user = "root";
    private static final String password = "786890";

    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;

    private int number;
    private int[][] field;


    public db_life(int n, int m){
        super(n,m);
        LENGHT = n;
        field = new int[n][n];
        String query = "select * from life";

        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(url, user, password);

            // getting Statement object to execute query
            stmt = con.createStatement();

            // executing SELECT query
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                field[rs.getInt(1)][rs.getInt(2)] = 1;
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            //close connection ,stmt and resultset here
            try { con.close(); } catch(SQLException se) { /*can't do anything */ }
            try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
            try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        }
    }
    public int get_value(int x, int y){
        if(x >= 0 && y >= 0 && x < field.length && y < field.length) {
            return field[x][y];
        }
        else {
            return 0;
        }
    }

    public void set_value(int x, int y, int value){
        if(x >= 0 && y >= 0 && x < field.length && y < field.length) {
            field[x][y] = value;
        }
    }

    public void Life_cycle(){
        for(int i = 0; i < field.length; i++){
            for(int j = 0; j < field.length; j++){
                int sum = 0;
                for(int k = 0; k < 9; k++){
                    if(k != 4 && (get_value(i - 1 + k / 3, j - 1 + k % 3) & (ALIVE | GONNA_DIE)) > 0){
                        sum++;
                    }
                }
                if(field[i][j] == ALIVE && (sum < LOWER_LIVE_BORDER || sum > UPPER_LIVE_BORDER)){
                    field[i][j] = GONNA_DIE;
                }
                else
                if(field[i][j] == EMPTY &&  sum == BORN_BORDER){
                    field[i][j] = GONNA_LIVE;
                }
            }
        }
        for(int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = (field[i][j] & (GONNA_LIVE | ALIVE)) % 3;
            }
        }
    }

    public void Save(){
        String delete_str = "DELETE FROM life;";

        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(url, user, password);

            // getting Statement object to execute query
            stmt = con.createStatement();

            // executing SELECT query
            stmt.executeUpdate(delete_str);

//            while (rs.next()) {
//               // field[rs.getInt(1)][rs.getInt(2)] = 1;
//            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
//        } finally {
//            //close connection ,stmt and resultset here
//            try { con.close(); } catch(SQLException se) { /*can't do anything */ }
//            try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
//            //try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
//        }

        for(int i = 0; i < LENGHT; i++){
            for(int j = 0; j < LENGHT; j++){
                if(field[i][j] != 0) {
                    String insert_str = "INSERT INTO life VALUES(" + i + "," + j +");";
                    try {
                        stmt.executeUpdate(insert_str);
                    }catch (SQLException sqlEx) {
                        sqlEx.printStackTrace();
                    }
                }
            }
        }
        try { con.close(); } catch(SQLException se) { /*can't do anything */ }
        try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
    }

    public void loadLife(){
        field = new int[LENGHT][LENGHT];
        String query = "select * from life";

        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(url, user, password);

            // getting Statement object to execute query
            stmt = con.createStatement();

            // executing SELECT query
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                field[rs.getInt(1)][rs.getInt(2)] = 1;
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            //close connection ,stmt and resultset here
            try { con.close(); } catch(SQLException se) { /*can't do anything */ }
            try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
            try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        }
    }

    public void newLife(int m){
        field = new int[LENGHT][LENGHT];

        Random R = new Random();
        int i = 0;
        int x;
        int y;
        while(i < m){
            x = Math.abs(R.nextInt()) % LENGHT;
            y = Math.abs(R.nextInt()) % LENGHT;

            if(field[x][y] == EMPTY){
                i++;
                field[x][y] = ALIVE;
            }
        }
    }
}
