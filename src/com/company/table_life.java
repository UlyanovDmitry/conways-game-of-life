package com.company;
import java.util.Random;

public class table_life extends Life{
    private int[][] field;

    private int EMPTY = 0;
    private int ALIVE = 1;
    private int GONNA_DIE = 2;
    private int GONNA_LIVE = 4;

    private int UPPER_LIVE_BORDER = 3;
    private int LOWER_LIVE_BORDER = 2;

    private int BORN_BORDER = 3;

    public table_life(int n, int m){
        super(n,m);
        field = new int[n][n];

        Random R = new Random();
        int i = 0;
        int x;
        int y;
        while(i < m){
            x = Math.abs(R.nextInt()) % n;
            y = Math.abs(R.nextInt()) % n;

            if(field[x][y] == EMPTY){
                i++;
                field[x][y] = ALIVE;
            }
        }
    }

    public int get_value(int x, int y){
        if(x >= 0 && y >= 0 && x < field.length && y < field.length) {
            return field[x][y];
        }
        else {
            return 0;
        }
    }

    public void set_value(int x, int y, int value){
        if(x >= 0 && y >= 0 && x < field.length && y < field.length) {
            field[x][y] = value;
        }
    }

    public void Life_cycle(){
        for(int i = 0; i < field.length; i++){
            for(int j = 0; j < field.length; j++){
                int sum = 0;
                for(int k = 0; k < 9; k++){
                    if(k != 4 && (get_value(i - 1 + k / 3, j - 1 + k % 3) & (ALIVE | GONNA_DIE)) > 0){
                        sum++;
                    }
                }
                if(field[i][j] == ALIVE && (sum < LOWER_LIVE_BORDER || sum > UPPER_LIVE_BORDER)){
                    field[i][j] = GONNA_DIE;
                }
                else
                if(field[i][j] == EMPTY &&  sum == BORN_BORDER){
                    field[i][j] = GONNA_LIVE;
                }
            }
        }
        for(int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = (field[i][j] & (GONNA_LIVE | ALIVE)) % 3;
            }
        }
    }
}
