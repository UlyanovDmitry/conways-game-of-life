package com.company;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.Timer;
/**
 * Created by ulyan on 23.03.2016.
 */
public class Window extends JFrame{

    int LENGHT = 50;
    private Cell[][] cell;

    int windowWidth;
    int windowHeight;
    JButton start_btn;
    JButton stop_btn;
    JButton next_step_btn;
    JButton clear_btn;
    JButton random_btn;
    JMenuBar menu;

    Timer timer;
    Field f;
    Controller c;

    public Window(int w, int h, Life l) {
        super("table_life");
        super.isDoubleBuffered();
        int width = (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        int height = (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight();

        windowWidth = w >= width ? width : w;
        windowHeight = h >= height ? height : h;

        this.setBounds((width - windowWidth)/2, (height - windowHeight)/2,windowWidth,windowHeight);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container manCont = this.getContentPane();
        manCont.setLayout(new BorderLayout());

        f = new Field(w,h,l.LENGHT);
        c = new Controller(l,f);

        manCont.add(f,BorderLayout.CENTER);

        Container button_container = new Container();
        button_container.setLayout(new GridLayout(1,5));

        start_btn = new JButton("Start");
        start_btn.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                c.SynchronizeFromViewToModel();
                timer.start();
            }
            @Override
            public void mousePressed(MouseEvent e) {}
            @Override
            public void mouseReleased(MouseEvent e) {}
            @Override
            public void mouseEntered(MouseEvent e) {}
            @Override
            public void mouseExited(MouseEvent e) {}
        });
        start_btn.setSize(new Dimension(100,60));

        stop_btn = new JButton("Stop");
        stop_btn.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                c.SynchronizeFromModelToView();
                timer.stop();
            }
            @Override
            public void mousePressed(MouseEvent e) {}
            @Override
            public void mouseReleased(MouseEvent e) {}
            @Override
            public void mouseEntered(MouseEvent e) {}
            @Override
            public void mouseExited(MouseEvent e) {}
        });
        stop_btn.setSize(new Dimension(100,60));

        next_step_btn = new JButton("Next");
        next_step_btn.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                c.SynchronizeFromViewToModel();
                c.Simulate();
            }
            @Override
            public void mousePressed(MouseEvent e) {}
            @Override
            public void mouseReleased(MouseEvent e) {}
            @Override
            public void mouseEntered(MouseEvent e) {}
            @Override
            public void mouseExited(MouseEvent e) {}
        });
        next_step_btn.setSize(new Dimension(100,60));

        clear_btn = new JButton("Clear");
        clear_btn.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                timer.stop();
                c.create_life(0);
            }
            @Override
            public void mousePressed(MouseEvent e) {}
            @Override
            public void mouseReleased(MouseEvent e) {}
            @Override
            public void mouseEntered(MouseEvent e) {}
            @Override
            public void mouseExited(MouseEvent e) {}
        });
        clear_btn.setSize(new Dimension(100,60));

        random_btn = new JButton("Random");
        random_btn.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                timer.stop();
                c.create_life(1);
                //c.life_model.Save();
            }
            @Override
            public void mousePressed(MouseEvent e) {}
            @Override
            public void mouseReleased(MouseEvent e) {}
            @Override
            public void mouseEntered(MouseEvent e) {}
            @Override
            public void mouseExited(MouseEvent e) {}
        });
        random_btn.setSize(new Dimension(100,60));

        timer = new Timer(80, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                c.Simulate();
            }
        });


        button_container.add(start_btn);
        button_container.add(stop_btn);
        button_container.add(next_step_btn);
        button_container.add(clear_btn);
        button_container.add(random_btn);

        manCont.add(button_container,BorderLayout.SOUTH);

        menu = new JMenuBar();
        JMenu setMenu = new JMenu("Settings");
        JMenuItem setMenuItem = new JMenuItem("Set params");
        setMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            }
        });
        setMenu.add(setMenuItem);
        menu.add(setMenu);
        this.setJMenuBar(menu);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                c.SynchronizeFromViewToModel();
                c.life_model.Save();
                super.windowClosing(e);
            }
        });
    }
}
