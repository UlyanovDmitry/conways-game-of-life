package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by ulyan on 23.03.2016.
 */
public class Cell extends JPanel {

    int statement;
    int numberOfStates;
    boolean clickable;

    public Cell(int w, int h, int states) {
        super();
        statement = 0;
        clickable = true;
        numberOfStates = states;
        this.setPreferredSize(new Dimension(w, h));
        this.setBackground(new Color(255,0,0));
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Click();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }
    public void SetStatement(int s){
        statement = s % numberOfStates;
        setBackground(new Color(255 - (int)(255 * (double)statement / numberOfStates), 0, (int)(255 * (double)statement / numberOfStates)));
    }
    public void Click(){
        if(clickable) {
            SetStatement(statement+1);
        }
    }

    public void set_new_size(int w,int h){
        this.setPreferredSize(new Dimension(w, h));
    }
}
