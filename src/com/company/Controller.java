package com.company;

/**
 * Created by ulyan on 29.03.2016.
 */
public class Controller {
    public Life life_model;
    public Field life_view;
    public int LENGHT;

    public Controller(Life l, Field f){
        LENGHT = l.LENGHT;
        life_view = f;
        life_model = l;
        SynchronizeFromModelToView();
    }

    public void SynchronizeFromModelToView(){
        for (int i = 0; i < LENGHT; i++) {
            for (int j = 0; j < LENGHT; j++) {
                if (life_model.get_value(i, j) == 1) {
                    life_view.cells[i][j].SetStatement(1);
                } else
                    life_view.cells[i][j].SetStatement(0);
            }
        }
    }

    public void SynchronizeFromViewToModel(){
        for (int i = 0; i < LENGHT; i++) {
            for (int j = 0; j < LENGHT; j++) {
                if (life_view.get_value(i, j) == 1) {
                    life_model.set_value(i,j,1);
                } else
                    life_model.set_value(i,j,0);
            }
        }
    }

    public void Simulate(){
        life_model.Life_cycle();
        SynchronizeFromModelToView();
    }

    public void create_life(int ratio){
        life_model.newLife(ratio * LENGHT*LENGHT/6);
        SynchronizeFromModelToView();
    }
}
