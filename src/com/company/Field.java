package com.company;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

/**
 * Created by ulyan on 29.03.2016.
 */
public class Field extends Container {
    public Cell[][] cells;
    int length;

    public Field(int h, int w, int number){
        super();
        length = (w < h) ? w : h;
        cells = new Cell[number][number];
        for(int i =0; i < number; i ++)
            for(int j = 0; j < number; j++){
                cells[i][j] = new Cell(200,200,2);
                this.add(cells[i][j]);
            }

        this.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
                set_new_size(Field.super.getWidth(),Field.super.getHeight());
            }

            @Override
            public void componentMoved(ComponentEvent e) {}

            @Override
            public void componentShown(ComponentEvent e) {}

            @Override
            public void componentHidden(ComponentEvent e) {}
        });
    }

    public int get_value(int x, int y){
        if(x >= 0 && y >= 0 && x < cells.length && y < cells.length) {
            return cells[x][y].statement;
        }
        else {
            return 0;
        }
    }

    public int get_lenght(){
        return cells.length;
    }

    public void set_new_size(int w, int h){
        int number = cells.length;
        length = (w < h) ? w : h;
        int cell_length = length / number;
        int border = cell_length/10;
//        Insets insets = this.getInsets();
        int indentX = (w - length)/2;
        int indentY = (h - length)/2;
        for(int i =0; i < number; i ++)
            for(int j = 0; j < number; j++){
                cells[i][j].set_new_size(cell_length - border,cell_length - border);
                cells[i][j].setBounds(cell_length * i + border + indentX, cell_length * j + border + indentY, cell_length - border, cell_length - border);
            }
    }
}
